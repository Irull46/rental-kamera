<?php

namespace App\Http\Controllers;

use App\Models\Kameradipinjam;
use Illuminate\Http\Request;

class KameradipinjamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kameradipinjam()
    {
        return view('kameradipinjam');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kameradipinjam  $kameradipinjam
     * @return \Illuminate\Http\Response
     */
    public function show(Kameradipinjam $kameradipinjam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kameradipinjam  $kameradipinjam
     * @return \Illuminate\Http\Response
     */
    public function edit(Kameradipinjam $kameradipinjam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kameradipinjam  $kameradipinjam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kameradipinjam $kameradipinjam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kameradipinjam  $kameradipinjam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kameradipinjam $kameradipinjam)
    {
        //
    }
}
