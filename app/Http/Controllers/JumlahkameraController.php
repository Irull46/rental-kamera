<?php

namespace App\Http\Controllers;

use App\Models\Jumlahkamera;
use Illuminate\Http\Request;

class JumlahkameraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jumlahkamera()
    {
        return view('jumlahkamera');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jumlahkamera  $jumlahkamera
     * @return \Illuminate\Http\Response
     */
    public function show(Jumlahkamera $jumlahkamera)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jumlahkamera  $jumlahkamera
     * @return \Illuminate\Http\Response
     */
    public function edit(Jumlahkamera $jumlahkamera)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Jumlahkamera  $jumlahkamera
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jumlahkamera $jumlahkamera)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jumlahkamera  $jumlahkamera
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jumlahkamera $jumlahkamera)
    {
        //
    }
}
