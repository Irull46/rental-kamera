<?php

namespace App\Http\Controllers;

use App\Models\Kameratersisa;
use Illuminate\Http\Request;

class KameratersisaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kameratersisa()
    {
        return view('kameratersisa');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kameratersisa  $kameratersisa
     * @return \Illuminate\Http\Response
     */
    public function show(Kameratersisa $kameratersisa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kameratersisa  $kameratersisa
     * @return \Illuminate\Http\Response
     */
    public function edit(Kameratersisa $kameratersisa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kameratersisa  $kameratersisa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kameratersisa $kameratersisa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kameratersisa  $kameratersisa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kameratersisa $kameratersisa)
    {
        //
    }
}
