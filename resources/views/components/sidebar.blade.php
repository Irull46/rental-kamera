<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="/">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Rental Kamera</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white {{ request()->is('/') ? 'active bg-gradient-primary' : '' }}" href="/">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Beranda</span>
          </a>
        </li>
        @can('admin')
          <li class="nav-item">
            <a class="nav-link text-white {{ request()->is('jumlahkamera') ? 'active bg-gradient-primary' : '' }}" href="/jumlahkamera">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">table_view</i>
              </div>
              <span class="nav-link-text ms-1">Jumlah Kamera</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white {{ request()->is('kameratersisa') ? 'active bg-gradient-primary' : '' }}" href="/kameratersisa">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">table_view</i>
              </div>
              <span class="nav-link-text ms-1">Kamera Tersisa</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white {{ request()->is('permohonan') ? 'active bg-gradient-primary' : '' }}" href="/permohonan">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">table_view</i>
              </div>
              <span class="nav-link-text ms-1">Permohonan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white {{ request()->is('kameradipinjam') ? 'active bg-gradient-primary' : '' }}" href="/kameradipinjam">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">table_view</i>
              </div>
              <span class="nav-link-text ms-1">Kamera Dipinjam</span>
            </a>
          </li>
        @endcan
        @can('pelanggan')
        <li class="nav-item">
          <a class="nav-link text-white {{ request()->is('peminjaman') ? 'active bg-gradient-primary' : '' }}" href="/peminjaman">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">table_view</i>
            </div>
            <span class="nav-link-text ms-1">Peminjaman</span>
          </a>
        </li>
        @endcan
        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Halaman akun</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white " href="/profil">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Profil</span>
          </a>
        </li>
      </ul>
    </div>
  </aside>