<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\JumlahkameraController;
use App\Http\Controllers\KameratersisaController;
use App\Http\Controllers\PermohonanController;
use App\Http\Controllers\KameradipinjamController;
use App\Http\Controllers\PeminjamanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/jumlahkamera', [JumlahkameraController::class, 'jumlahkamera'])->middleware('verifikasi');
Route::get('/kameratersisa', [KameratersisaController::class, 'kameratersisa'])->middleware('verifikasi');
Route::get('/permohonan', [PermohonanController::class, 'permohonan'])->middleware('verifikasi');
Route::get('/kameradipinjam', [KameradipinjamController::class, 'kameradipinjam'])->middleware('verifikasi');
Route::get('/peminjaman', [PeminjamanController::class, 'peminjaman'])->middleware('pelanggan');

Route::get('/profil', function () {
    return view('profil');
});

